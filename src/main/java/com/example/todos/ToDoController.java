package com.example.todos;

import com.example.rest.DataResponse;
import com.example.rest.MessageResponse;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping(value = "/api")
public class ToDoController {
    private final AtomicLong counter = new AtomicLong();
    private static Map<Long, ToDo> todos = new HashMap<>();
    static {
        todos.put(1L, new ToDo(1, "My First todo", 1));
        todos.put(2L, new ToDo(2, "My Second todo", 2));
        todos.put(3L, new ToDo(3, "My Third todo", 3));
    }

    @RequestMapping("/todos")
    public DataResponse todos() {
        return new DataResponse(todos.values());
    }

    @RequestMapping("/todos/{id}")
    public DataResponse ToDoById(@PathVariable("id") long id) {
        return new DataResponse(todos.get(id));
    }

    @RequestMapping(value = "/todos/{id}", method = DELETE)
    public MessageResponse deleteToDoById(@PathVariable("id") long id) {
        todos.remove(id);
        return new MessageResponse("Resource deleted");
    }

    @RequestMapping(value = "/todos", method = POST)
    public MessageResponse createToDo(@RequestBody final ToDo todo) {
        todos.put(todo.getId(), todo);
        return new MessageResponse("Resource deleted");
    }
}
