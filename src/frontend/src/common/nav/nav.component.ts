import {Component} from '@angular/core';
import {AuthenticationService} from '../../user/services/authentication.service';

@Component({
    selector: 'jj-nav',
    templateUrl: './nav.component.html',
    styleUrls: ['./nav.component.scss']
})
export class NavComponent {
    constructor(private authSvc: AuthenticationService) {}
}


