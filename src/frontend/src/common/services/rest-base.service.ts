import {Injectable} from '@angular/core';
import {Http, Response, RequestOptionsArgs, Headers} from '@angular/http';
import {Observable} from 'rxjs/Rx';

@Injectable()
export class RESTBase {
    private baseUrl = API_URL;
    private authHeader: Headers;
    public path: String;

    constructor(public http: Http) {
        this.authHeader = new Headers();
        this.authHeader.append('Authorization', 'Basic ' + btoa('user' + ':' + 'password'));
    }

    create(model: Object): Observable<RESTSuccess> {
        return this.http.post(this.getUrl(), model, { withCredentials: true })
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    find<T>(options?: RequestOptionsArgs): Observable<T[]> {
        return this.http.get(this.getUrl(), {withCredentials: true })
            .map((res: Response) => res.json().data)
            .catch((error: any) => Observable.throw(error));
    }

    findOne<T>(id: any): Observable<T> {
        return this.http.get(this.getUrl(), {withCredentials: true })
            .map((res: Response) => res.json().data)
            .catch((error: any) => Observable.throw(error));
    }

    update(id: any, model: Object): Observable<RESTSuccess> {
        return this.http.post(this.getUrl(id), model, {withCredentials: true })
            .map((res: Response) => res.json())
            .catch((error: any) => {
                return Observable.throw(error);
            });
    }

    delete(id: any): Observable<RESTSuccess> {
        return this.http.delete(this.getUrl(id), { withCredentials: true })
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    private getUrl(id?: any): string {
        if (!id) {
            return `${this.baseUrl}/api/${this.path}`;
        }
        return `${this.baseUrl}/api/${this.path}/${id}`;
    }
}

interface RESTSuccess {
    message: string;
    creationId?: number;
}
