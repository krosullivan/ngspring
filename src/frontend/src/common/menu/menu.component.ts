import {Component} from '@angular/core';
@Component({
    selector: 'jj-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss']
})
export class MenuComponent {
    navItems: INavItem[];
    constructor() {
        this.navItems = [
            {
                title: 'ToDos',
                routerLink: '/todos'
            }
        ];
    }
}

interface INavItem {
    title: string;
    routerLink: string;
}
