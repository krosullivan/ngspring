import {Component, ViewEncapsulation} from '@angular/core';
import './styles/main.scss';
import {AuthenticationService} from '../user/services/authentication.service';

@Component({
    selector: 'jj-app',
    templateUrl: './app.component.html',
    styleUrls: ['./styles/main.scss', './app.component.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: [AuthenticationService]
})

export class AppComponent {
    constructor(private authSvc: AuthenticationService) {}
}
