import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {HttpModule, JsonpModule} from '@angular/http';
import {AppComponent} from './app.component';
import {CommonModule} from '../common/common.module';
import {NavComponent} from '../common/nav/nav.component';
import {MenuComponent} from '../common/menu/menu.component';
import {ToDosModule} from '../todos/todos.module';
import {UserModule} from '../user/user.module';

@NgModule({
    imports: [
        BrowserModule,
        RouterModule.forRoot([
            {
                path: '',
                redirectTo: 'todos',
                pathMatch: 'full'
            }
        ]),
        HttpModule,
        JsonpModule,
        UserModule,
        CommonModule,
        ToDosModule,
    ],
    declarations: [
        AppComponent,
        NavComponent,
        MenuComponent
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
