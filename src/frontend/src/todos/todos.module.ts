import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {ToDoComponent} from './components/todos.component';
import {AuthGuard} from '../user/services/auth-guard.service';
@NgModule({
    imports: [
        BrowserModule,
        RouterModule.forRoot([
            {
                path: 'todos',
                component: ToDoComponent,
                canActivate: [AuthGuard]
            }
        ])
    ],
    declarations: [ToDoComponent],
    providers: [AuthGuard]
})
export class ToDosModule {}
