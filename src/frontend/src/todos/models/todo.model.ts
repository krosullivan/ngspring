export class ToDo {
    constructor(public id: number,
                public description: string,
                public priority: number) {
    }
}