import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService} from '../services/authentication.service';

@Component({
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent {
    message: string;
    username: string;
    password: string;

    constructor(public authService: AuthenticationService, public router: Router) {
        this.setMessage();
    }
    setMessage() {
        this.message = 'Logged ' + (this.authService.isLoggedIn() ? 'in' : 'out');
    }
    login() {
        this.message = 'Trying to log in ...';
        this.authService.login(this.username, this.password).subscribe(() => {
            this.setMessage();
            if (this.authService.isLoggedIn()) {
                let redirect = this.authService.redirectUrl ? this.authService.redirectUrl : '/';
                this.router.navigate([redirect]);
            }
        });
    }
}
