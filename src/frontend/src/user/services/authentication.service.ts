import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Http, Response, Headers} from '@angular/http';

@Injectable()
export class AuthenticationService {
    redirectUrl: string;

    constructor(private http: Http) {}

    logout() {
        this.http.get(`${API_URL}/logout`, {withCredentials: true})
            .catch((error: any) => {
                return Observable.throw(error);
            }).finally(() => {
                this.setLoggedIn(false);
                location.reload();
            })
            .subscribe();
    }

    login(username: string, password: string): Observable<LoginResponse>{
        let authHeader = new Headers();
        authHeader.append('Authorization', 'Basic ' + btoa(username + ':' + password));
        return this.http.get(`${API_URL}/user`, {headers: authHeader, withCredentials: true})
            .map((res: Response) => this.setLoggedIn(true))
            .catch((error: any) => {
                this.setLoggedIn(false);
                return Observable.throw(error);
            });
    }

    checkAuthentication(): Observable<boolean> {
        return this.http.get(`${API_URL}/user`, { withCredentials: true })
            .map((res: Response) => this.setLoggedIn(true))
            .catch((error: any) => {
                this.logout();
                return Observable.throw(error);
            });
    }

    setLoggedIn(value: boolean) {
        localStorage.setItem('authenticated', JSON.stringify(value));
    }

    isLoggedIn(): boolean {
        return JSON.parse(localStorage.getItem('authenticated'));
    }

}

interface LoginResponse {
    authenticate: boolean;
}
