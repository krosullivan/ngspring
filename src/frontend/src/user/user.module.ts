import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {LoginComponent} from './components/login.component';
import {AuthenticationService} from './services/authentication.service';
import {FormsModule} from '@angular/forms';
import {LoggedInGuard} from './services/loggedin-gurad.service';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        RouterModule.forRoot([
            {
                path: 'login',
                component: LoginComponent,
                canActivate: [LoggedInGuard]
            }
        ])
    ],
    providers: [AuthenticationService, LoggedInGuard],
    declarations: [LoginComponent]
})

export class UserModule {
}
